"use strict"

// 1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// 2) Для чого потрібно викликати super() у конструкторі класу-нащадка?

// 1) Якщо ми робимо об'єкт прототипом для іншого об'єкту, то цей "інший" об'єкт отримує всі методі і властивості прототипу, також якщо ми звернемося до якоїсь властивості об'єкту, а її в ньому не буде, то JavaScript візьме її з прототипу.

// 2) Для того, щоб передати аргументи класу-нащадку і він успадкуваав властивості та методи класу-батька.

// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {

    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name};
    set name (name) {
        this._name = name
    };

    get age() {
        return this._age}
    set age (age) {
        this._age = age
    };

    get salary() {
        return this._salary};
    set salary (salary) {
        this._salary = salary
    };
}

 class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return (this._salary * 3)};

     set salary (salary) {
            this._salary = salary
        };
 }

 const user1 = new Programmer ("Sanchizes", "55", "2000", "english");
 console.log(user1);
 console.log(user1.salary);

 const user2 = new Programmer ("Sanchello", "56", "1000", "english, chinese");
 console.log(user2);
 console.log(user2.salary);

 const user3 = new Programmer ("Sanchoys", "57", "1500", "english, polish, elvish");
 console.log(user3);
 console.log(user3.salary);

 const user4 = new Programmer ("Sancho Panza", "55", "10", "spanish");
 console.log(user4);
 console.log(user4.salary);